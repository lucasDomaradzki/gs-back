## PASSWORD SYSTEM BACK-END

![Java 8](https://img.shields.io/badge/java-8-brightgreen.svg)
![SpringBoot 4](https://img.shields.io/badge/springboot-4-brightgreen.svg)
![Maven 3](https://img.shields.io/badge/maven-3.3.9-brightgreen.svg)
![Postgre](https://img.shields.io/badge/postgre-11.4-brightgreen.svg)

### Small business aplication to generate and supervise passwords

### Required tecnologies to run this project:
- Java 8
- SpringBoot 4
- Maven 3
- Postgre Server 11.4

Once the project is cloned and imported as a maven project into SpringBoot and dependencies are downloaded, run the commands bellow to install Postgre as the database:
- sudo apt-get update
- sudo apt-get install postgresql postgresql-contrib

After installation, run the commands to set up the password to the default database and user created, named 'postgres':
This command bellow will log you into the server
- sudo -i -u postgres
This command allow Postgre prompt commands
- psql
Then set up the new password to user 'postgres' as **gs123** as already configured at resources configuration of the project:
- \password postgres

The DDL structure of the project  can be found at **gs-back/src/main/resources/sql**
There are DDL and DML scripts there to create the database structure and also a dml insert of a client(user) to run the system as admin

Then to start project run the tomcat server built-in to SpringBoot and the service will run on localhost at **port 9090**
