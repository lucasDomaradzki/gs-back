package br.com.api.repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.api.entity.ClientEntity;
import br.com.api.exception.NotFoundException;
import br.com.api.exception.ApiException;

/**
 * Repository class for Client
 * 
 * @author lucas
 */
@Repository
public class ClientRepository {

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Return ClientEntity by email
	 * 
	 * @param email String email
	 * @return ClientEntity
	 * @throws ApiException ClientEntity Not Found
	 */
	public ClientEntity findClientByEmail( final String email ) throws ApiException {
		try {
			final String jpql = "FROM ClientEntity where email = :email";
			final TypedQuery<ClientEntity> query = this.entityManager.createQuery( jpql, ClientEntity.class );
			query.setParameter( "email", email );
			return query.getSingleResult();
		} catch ( final NoResultException e ) {
			throw new NotFoundException( "Nenhum usuário encontrado para o email: " + email );
		}
	}

	/**
	 * Persists ClientEntity
	 * 
	 * @param client ClientEntity
	 */
	@Transactional( rollbackOn = ApiException.class )
	public void persistClient( final ClientEntity client ) {
		this.entityManager.persist( client );
	}
}
