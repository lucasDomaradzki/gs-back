package br.com.api.repository;

import static br.com.api.enums.PasswordStatus.CHAMADO;
import static br.com.api.enums.PasswordStatus.CRIADO;
import static br.com.api.enums.PasswordStatus.FINALIZADO;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import br.com.api.domain.Password;
import br.com.api.entity.PasswordEntity;
import br.com.api.enums.PasswordStatus;
import br.com.api.enums.PasswordType;
import br.com.api.exception.NotFoundException;
import br.com.api.exception.ApiException;

/**
 * Repository Class for Password
 * 
 * @author lucas
 */
@Repository
public class PasswordRepository {

	private static final int MAX_RESULTS = 10;

	private static final String NA = "N.A.";

	private static final Integer ONE = 1;

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Set the password as inactive and returns the next one to be called
	 * 
	 * @param value Long Password value
	 * @param type PasswordType Password type
	 * @return ResponseEntity<Password> Nex password to be called
	 * @throws ApiException Error fetching the password
	 */
	@Transactional( rollbackOn = ApiException.class )
	public ResponseEntity<Password> callPassword( final Long value, final PasswordType type ) throws ApiException {
		this.resetCalledPassword( value, type );
		return this.findNextPasswordToBeCalled();
	}

	/**
	 * Creates a new password by its PasswordType
	 * 
	 * @param type PasswordType Password type
	 * @return ResponseEntity<Password> New password created
	 */
	@Transactional( rollbackOn = ApiException.class )
	public ResponseEntity<Password> createNewPassword( final PasswordType type ) {
		final Integer latestPassword = this.findLatestPasswordId( type );
		final PasswordEntity passwordEntity = new PasswordEntity();

		final Integer nextPassword = latestPassword + ONE;

		passwordEntity.setActive( true );
		passwordEntity.setStatus( CRIADO );
		passwordEntity.setPasswordType( type );
		passwordEntity.setPasswordValue( nextPassword );

		this.entityManager.persist( passwordEntity );

		final Password password = new Password();
		password.setValue( passwordEntity.getPasswordCombination() );

		return ResponseEntity.status( HttpStatus.ACCEPTED ).body( password );
	}

	/**
	 * Find all active passwords by its type
	 * 
	 * @param type PasswordType Password type
	 * @return ResponseEntity<List<String>> List of passwords
	 */
	public ResponseEntity<List<String>> findAllPasswordsByType( final PasswordType type ) {
		final String jpql = "FROM ClientPasswordEntity password WHERE password.active = :active AND password.passwordType = :type ORDER BY password.value";
		final TypedQuery<PasswordEntity> query = this.entityManager.createQuery( jpql, PasswordEntity.class );

		query.setParameter( "active", true );
		query.setParameter( "type", type );

		final List<PasswordEntity> passwords = query.setMaxResults( MAX_RESULTS ).getResultList();

		if ( passwords == null || passwords.isEmpty() ) {
			return ResponseEntity.status( HttpStatus.NO_CONTENT ).build();
		}

		final List<String> passwordList = new LinkedList<>();
		for ( PasswordEntity password : passwords ) {
			passwordList.add( password.getPasswordValue().toString() );
		}

		return ResponseEntity.status( HttpStatus.OK ).body( passwordList );
	}

	/**
	 * Return the current (MAX) password by its type
	 * 
	 * @param type PasswordType Password type
	 * @return ResponseEntity<Password> Current password
	 */
	public ResponseEntity<Password> findCurrentPassword( final PasswordType type ) {
		final PasswordEntity passwordBD = this.findLatestPasswordByType( type );
		final Password password = new Password();
		if ( passwordBD == null ) {
			password.setValue( NA );
			return ResponseEntity.status( HttpStatus.OK ).body( password );
		}

		password.setValue( passwordBD.getPasswordCombination() );
		return ResponseEntity.status( HttpStatus.OK ).body( password );
	}

	/**
	 * Return the current ClientPasswordEntity by its type
	 * 
	 * @param type PasswordType Password type
	 * @return ClientPasswordEntity
	 */
	private PasswordEntity findLatestPasswordByType( final PasswordType type ) {
		try {
			final String jpql = "FROM ClientPasswordEntity password WHERE password.active = :active AND password.passwordType = :passwordType ORDER BY password.id DESC";
			final TypedQuery<PasswordEntity> query = this.entityManager.createQuery( jpql, PasswordEntity.class );
			query.setParameter( "active", true );
			query.setParameter( "passwordType", type );

			final List<PasswordEntity> passwordList = query.getResultList();
			if ( passwordList == null || passwordList.isEmpty() ) {
				return null;
			}
			return passwordList.get( 0 );
		} catch ( final NoResultException e ) {
			return null;
		}
	}

	/**
	 * Find the MAX Integer value of a password by its type and return 0 when there's no MAX value
	 * 
	 * @param type PasswordType Password type
	 * @return Integer Password value
	 */
	public Integer findLatestPasswordId( final PasswordType type ) {
		try {
			final String jpql = "SELECT MAX( password.VALUE ) FROM CLIENTPASSWORD password WHERE password.PASSWORDTYPE = :passwordType AND password.DSSTATUS <> :status";
			final Query query = this.entityManager.createNativeQuery( jpql );
			query.setParameter( "passwordType", type.toString() );
			query.setParameter( "status", PasswordStatus.FINALIZADO.toString() );

			final Integer lastPassword = (Integer) query.getSingleResult();
			if ( lastPassword == null ) {
				return 0;
			}
			return lastPassword;
		} catch ( final NoResultException e ) {
			return 0;
		}
	}

	/**
	 * Returns the new password to be called
	 * 
	 * @return ResponseEntity<Password> next Password
	 */
	public ResponseEntity<Password> findNextPasswordToBeCalled() {
		final String jpql = "FROM ClientPasswordEntity WHERE active = :active AND status = :status ORDER BY passwordType DESC, value";
		final TypedQuery<PasswordEntity> query = this.entityManager.createQuery( jpql, PasswordEntity.class );

		query.setParameter( "active", true );
		query.setParameter( "status", CRIADO );

		final List<PasswordEntity> passwords = query.getResultList();

		if ( passwords == null || passwords.isEmpty() ) {
			return ResponseEntity.status( HttpStatus.OK ).body( new Password( NA, "" ) );
		}

		final PasswordEntity passwordEntity = passwords.get( 0 );

		final Password password = new Password();

		password.setValue( passwordEntity.getPasswordValue().toString() );
		password.setPasswordType( passwordEntity.getPasswordType() );
		password.setPasswordTypePrefix( passwordEntity.getPasswordType().getPrefix() );

		return ResponseEntity.status( HttpStatus.OK ).body( password );
	}

	/**
	 * Find password by its value and type
	 * 
	 * @param value Long Password value
	 * @param type PasswordType Password type
	 * @return ClientPasswordEntity
	 * @throws ApiException Password Not Found
	 */
	private PasswordEntity findPasswordByValueAndType( final Long value, final PasswordType type ) throws ApiException {
		try {
			final String jpql = "FROM ClientPasswordEntity WHERE passwordType = :type AND value = :value AND active = :active";
			final TypedQuery<PasswordEntity> query = this.entityManager.createQuery( jpql, PasswordEntity.class );

			query.setParameter( "type", type );
			query.setParameter( "value", value.intValue() );
			query.setParameter( "active", true );

			return query.getSingleResult();
		} catch ( final NoResultException e ) {
			throw new NotFoundException( "Erro ao buscar senha: " + type.getPrefix() + value );
		}
	}

	/**
	 * Reset all passwords
	 * 
	 * @return ResponseEntity<?> Password reset status
	 */
	@Transactional( rollbackOn = ApiException.class )
	public ResponseEntity<?> resetAllPasswords() {
		final String jpql = "UPDATE ClientPasswordEntity SET active = :active, status = :status, updateDate = :updateDate";
		final Query query = this.entityManager.createQuery( jpql );

		query.setParameter( "active", false );
		query.setParameter( "status", FINALIZADO );
		query.setParameter( "updateDate", LocalDateTime.now() );

		query.executeUpdate();

		return ResponseEntity.status( HttpStatus.OK ).build();
	}

	/**
	 * Reset the password by its value and type
	 * 
	 * @param value Long Password value
	 * @param type PasswordType Password type
	 * @throws ApiException Password Not Found
	 */
	private void resetCalledPassword( final Long value, final PasswordType type ) throws ApiException {
		final PasswordEntity password = this.findPasswordByValueAndType( value, type );

		password.setActive( false );
		password.setStatus( CHAMADO );
		password.setUpdateDate( LocalDateTime.now() );

		this.entityManager.persist( password );
	}

}
