package br.com.api.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import br.com.api.enums.ClientRole;

/**
 * Database table representation for CLIENT
 *
 * @author lucas
 */
@Entity
@Table( name = "CLIENT" )
public class ClientEntity implements Serializable {

	private static final long serialVersionUID = -7736096571912411360L;

	@Column( name = "EMAIL" )
	private String email;

	@Id
	@Column( name = "CLIENTID" )
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;

	@Column( name = "DTISSUE" )
	private LocalDateTime issuedAt;

	@Column( name = "NAME" )
	private String name;

	@Column( name = "PASSWORD" )
	private String password;

	@Column( name = "PASSWORDEXPIRATION" )
	private LocalDateTime passwordExpiration;

	@Column( name = "ROLE" )
	@Enumerated( EnumType.STRING )
	private ClientRole role;

	@Column( name = "DTUPDATE" )
	private LocalDateTime updateDate;

	@PrePersist
	private void beforePersist() {
		this.issuedAt = LocalDateTime.now();
		this.updateDate = LocalDateTime.now();
	}

	@PreUpdate
	private void beforeUpdate() {
		this.updateDate = LocalDateTime.now();
	}

	public String getEmail() {
		return email;
	}

	public Long getId() {
		return id;
	}

	public LocalDateTime getIssuedAt() {
		return issuedAt;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public LocalDateTime getPasswordExpiration() {
		return passwordExpiration;
	}

	public ClientRole getRole() {
		return role;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public void setEmail( String email ) {
		this.email = email;
	}

	public void setIssuedAt( LocalDateTime issuedAt ) {
		this.issuedAt = issuedAt;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public void setPassword( String password ) {
		this.password = password;
	}

	public void setPasswordExpiration( LocalDateTime passwordExpiration ) {
		this.passwordExpiration = passwordExpiration;
	}

	public void setRole( ClientRole role ) {
		this.role = role;
	}

	public void setUpdateDate( LocalDateTime updateDate ) {
		this.updateDate = updateDate;
	}

}
