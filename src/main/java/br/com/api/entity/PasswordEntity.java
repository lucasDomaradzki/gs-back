package br.com.api.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import br.com.api.enums.PasswordStatus;
import br.com.api.enums.PasswordType;

/**
 * Database table representation for PASSWORD
 *
 * @author lucas
 */
@Entity
@Table( name = "PASSWORD" )
public class PasswordEntity implements Serializable {

	private static final long serialVersionUID = 3317704996205166718L;

	@Column( name = "FGACTIVE" )
	private boolean active;

	@Id
	@Column( name = "PASSWORDID" )
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;

	@Column( name = "DTISSUE" )
	private LocalDateTime issueAt;

	@Column( name = "PASSWORDTYPE" )
	@Enumerated( EnumType.STRING )
	private PasswordType passwordType;

	@Column( name = "VLPASSWORD" )
	private Integer passwordValue;

	@Column( name = "STATUS" )
	@Enumerated( EnumType.STRING )
	private PasswordStatus status;

	@Column( name = "DTUPDATE" )
	private LocalDateTime updateDate;

	@PrePersist
	private void beforePersist() {
		this.issueAt = LocalDateTime.now();
		this.updateDate = LocalDateTime.now();
	}

	@PreUpdate
	private void beforeUpdate() {
		this.updateDate = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public LocalDateTime getIssuedAt() {
		return issueAt;
	}

	public String getPasswordCombination() {
		return this.getPasswordType().getPrefix() + this.getPasswordValue();
	}

	public PasswordType getPasswordType() {
		return passwordType;
	}

	public Integer getPasswordValue() {
		return passwordValue;
	}

	public PasswordStatus getStatus() {
		return status;
	}

	public LocalDateTime getUpdateDate() {
		return updateDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive( boolean active ) {
		this.active = active;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public void setIssuedAt( LocalDateTime issueDate ) {
		this.issueAt = issueDate;
	}

	public void setPasswordType( PasswordType passwordType ) {
		this.passwordType = passwordType;
	}

	public void setPasswordValue( Integer value ) {
		this.passwordValue = value;
	}

	public void setStatus( PasswordStatus status ) {
		this.status = status;
	}

	public void setUpdateDate( LocalDateTime updateDate ) {
		this.updateDate = updateDate;
	}

}
