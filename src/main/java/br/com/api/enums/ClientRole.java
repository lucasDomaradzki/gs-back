package br.com.api.enums;

/**
 * Enum type for different types of Client
 * 
 * @author lucas
 */
public enum ClientRole {

	CLIENTE,

	GERENTE

}
