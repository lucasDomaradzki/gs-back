package br.com.api.enums;

/**
 * Enum Type for different types of Passwords
 * 
 * @author lucas
 */
public enum PasswordType {

	NORMAL( "N" ),

	PREFERENCIAL( "P" );

	private String prefix;

	private PasswordType( String prefix ) {
		this.prefix = prefix;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix( String prefix ) {
		this.prefix = prefix;
	}

}
