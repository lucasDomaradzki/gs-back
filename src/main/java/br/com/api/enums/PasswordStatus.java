package br.com.api.enums;

/**
 * Enum type for different types of PasswordTypes
 * 
 * @author lucas
 */
public enum PasswordStatus {

	CHAMADO,

	CRIADO,

	FINALIZADO

}
