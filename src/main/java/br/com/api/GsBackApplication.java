package br.com.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GsBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(GsBackApplication.class, args);
	}

}
