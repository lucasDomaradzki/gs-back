package br.com.api.service;

import org.springframework.http.ResponseEntity;

import br.com.api.domain.Client;
import br.com.api.exception.ApiException;

public interface ClientService {

	/**
	 * Log in Client to the system
	 * 
	 * @param client Client
	 * @return ResponseEntity<Entity> Client Logged In
	 * @throws ApiException Error attempting to log in client
	 */
	public ResponseEntity<?> logInClient( final Client client ) throws ApiException;
}
