package br.com.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.api.domain.Client;
import br.com.api.entity.ClientEntity;
import br.com.api.exception.ApiException;
import br.com.api.repository.ClientRepository;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepository repository;

	@Override
	public ResponseEntity<?> logInClient( final Client client ) throws ApiException {
		final ClientEntity clientBD = this.repository.findClientByEmail( client.getEmail() );
		// FIXME @lucascampos - FIX THIS THROUGH PROPER AUTHENTICATION
		if ( clientBD.getPassword().equals( client.getPassword() ) ) {

			this.repository.persistClient( clientBD );

			return ResponseEntity.status( HttpStatus.OK ).body( new Client( clientBD.getName() ) );
		}

		return ResponseEntity.status( HttpStatus.UNAUTHORIZED ).build();
	}
}
