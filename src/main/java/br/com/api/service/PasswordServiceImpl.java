package br.com.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.api.domain.Password;
import br.com.api.enums.PasswordType;
import br.com.api.exception.ApiException;
import br.com.api.repository.PasswordRepository;

@Service
public class PasswordServiceImpl implements PasswordService {

	@Autowired
	private PasswordRepository repository;

	@Override
	public ResponseEntity<Password> callPassword( final Long value, final PasswordType type ) throws ApiException {
		return this.repository.callPassword( value, type );
	}

	@Override
	public ResponseEntity<Password> createNewPassword( final PasswordType type ) throws ApiException {
		return this.repository.createNewPassword( type );
	}

	@Override
	public ResponseEntity<List<String>> findAllPasswordsByType( final PasswordType type ) throws ApiException {
		return this.repository.findAllPasswordsByType( type );
	}

	@Override
	public ResponseEntity<Password> findCurrentPassword( final PasswordType type ) throws ApiException {
		return this.repository.findCurrentPassword( type );
	}

	@Override
	public ResponseEntity<Password> findCurrentPasswordToBeCalled() throws ApiException {
		return this.repository.findNextPasswordToBeCalled();
	}

	@Override
	public ResponseEntity<?> resetAllPasswords() throws ApiException {
		return this.repository.resetAllPasswords();
	}
}
