package br.com.api.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import br.com.api.domain.Password;
import br.com.api.enums.PasswordType;
import br.com.api.exception.ApiException;

public interface PasswordService {

	/**
	 * Set the current password as inactive and returns the next one to be called
	 * 
	 * @param value Long Password value
	 * @param type PasswordType Password type
	 * @return ResponseEntity<Password> Next Password
	 * @throws ApiException Error fetching the next password
	 */
	public ResponseEntity<Password> callPassword( final Long value, final PasswordType type ) throws ApiException;

	/**
	 * Creates a new password by its type
	 * 
	 * @param type PasswordType
	 * @return ResponseEntity<Password> new password created
	 * @throws ApiException Error creating new password
	 */
	public ResponseEntity<Password> createNewPassword( final PasswordType type ) throws ApiException;

	/**
	 * Return the active password list of its type
	 * 
	 * @param type PasswordType Password type
	 * @return ResponseEntity<List<String>> List of passwords
	 * @throws ApiException Error fetching the the list of passwords
	 */
	public ResponseEntity<List<String>> findAllPasswordsByType( final PasswordType type ) throws ApiException;

	/**
	 * Find the current password by its type
	 * 
	 * @param type PasswordType
	 * @return ResponseEntity<Password> Current password
	 * @throws ApiException Error fetching the current password
	 */
	public ResponseEntity<Password> findCurrentPassword( final PasswordType type ) throws ApiException;

	/**
	 * Find the current password to be called
	 * 
	 * @return ResponseEntity<Password> Current password
	 * @throws ApiException Error fetching the current password to be called
	 */
	public ResponseEntity<Password> findCurrentPasswordToBeCalled() throws ApiException;

	/**
	 * Resets all passwords
	 * 
	 * @return ResponseEntity<?> Response status of the reset passwords
	 * @throws ApiException Error reseting passwords
	 */
	public ResponseEntity<?> resetAllPasswords() throws ApiException;
}
