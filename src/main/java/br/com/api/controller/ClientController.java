package br.com.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.domain.Client;
import br.com.api.exception.ApiException;
import br.com.api.service.ClientService;

@RestController
@RequestMapping( "/client" )
public class ClientController {

	@Autowired
	private ClientService clientService;

	@RequestMapping( value = "/login", method = RequestMethod.PATCH )
	public ResponseEntity<?> logInClient( @RequestBody final Client client ) throws ApiException {
		return this.clientService.logInClient( client );
	}

}
