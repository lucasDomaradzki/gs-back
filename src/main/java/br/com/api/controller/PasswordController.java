package br.com.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.domain.Password;
import br.com.api.enums.PasswordType;
import br.com.api.exception.ApiException;
import br.com.api.service.PasswordService;

@RestController
@RequestMapping( "/password" )
public class PasswordController {

	@Autowired
	private PasswordService service;

	@RequestMapping( value = "/reset", method = RequestMethod.PUT )
	public ResponseEntity<?> callPassword() throws ApiException {
		return this.service.resetAllPasswords();
	}

	@RequestMapping( value = "{value}", method = RequestMethod.PATCH )
	public ResponseEntity<Password> callPassword( @PathVariable( name = "value" ) final Long value, @RequestParam( name = "type" ) final PasswordType type ) throws ApiException {
		return this.service.callPassword( value, type );
	}

	@RequestMapping( method = RequestMethod.POST )
	public ResponseEntity<Password> createNewPasswordByType( @RequestParam( name = "type" ) final PasswordType type ) throws ApiException {
		return this.service.createNewPassword( type );
	}

	@RequestMapping( method = RequestMethod.GET )
	public ResponseEntity<List<String>> findAllRegularPasswords( @RequestParam( name = "type" ) final PasswordType type ) throws ApiException {
		return this.service.findAllPasswordsByType( type );
	}

	@RequestMapping( value = "/manage", method = RequestMethod.GET )
	public ResponseEntity<Password> findCurrentPasswordByType( @RequestParam( name = "type" ) final PasswordType type ) throws ApiException {
		return this.service.findCurrentPassword( type );
	}

	@RequestMapping( value = "/current", method = RequestMethod.GET )
	public ResponseEntity<Password> findCurrentPasswordToBeCalled() throws ApiException {
		return this.service.findCurrentPasswordToBeCalled();
	}
}
