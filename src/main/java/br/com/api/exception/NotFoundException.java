package br.com.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception when entity is not found
 * 
 * @author lucas
 */
@ResponseStatus( value = HttpStatus.NOT_FOUND )
public class NotFoundException extends ApiException {

	private static final long serialVersionUID = -906196588284501688L;

	public NotFoundException() {
		super();
	}

	public NotFoundException( final String message ) {
		super( message );
	}

}
