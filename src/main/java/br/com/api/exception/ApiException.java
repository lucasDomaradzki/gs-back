package br.com.api.exception;

/**
 * System generic exception
 * 
 * @author lucas
 */
public class ApiException extends Exception {

	private static final long serialVersionUID = -2224132684654378847L;

	public ApiException() {
		super();
	}

	public ApiException( final String message ) {
		super( message );
	}

}
