package br.com.api.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import br.com.api.enums.PasswordType;

/**
 * Password json object
 * 
 * @author lucas
 */
@JsonInclude( value = Include.NON_NULL )
public class Password implements Serializable {

	private static final long serialVersionUID = -8803454699326950704L;

	private PasswordType passwordType;

	private String passwordTypePrefix;

	private String value;

	public Password() {
		super();
	}

	public Password( final String value, final String passwordTypePrefix ) {
		super();
		this.value = value;
		this.passwordTypePrefix = passwordTypePrefix;
	}

	public PasswordType getPasswordType() {
		return passwordType;
	}

	public String getPasswordTypePrefix() {
		return passwordTypePrefix;
	}

	public String getValue() {
		return value;
	}

	public void setPasswordType( PasswordType passwordType ) {
		this.passwordType = passwordType;
	}

	public void setPasswordTypePrefix( String passwordTypePrefix ) {
		this.passwordTypePrefix = passwordTypePrefix;
	}

	public void setValue( String value ) {
		this.value = value;
	}

}
