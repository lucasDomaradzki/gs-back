package br.com.api.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Client json object
 * 
 * @author lucas
 */
@JsonInclude( value = Include.NON_NULL )
public class Client implements Serializable {

	private static final long serialVersionUID = 8116184135002640040L;

	private String clientName;

	private String email;

	private Long id;

	private String password;

	public Client() {
		super();
	}

	public Client( final String clientName ) {
		super();
		this.clientName = clientName;
	}

	public String getClientName() {
		return clientName;
	}

	public String getEmail() {
		return email;
	}

	public Long getId() {
		return id;
	}

	public String getPassword() {
		return password;
	}

	public void setClientName( String clientName ) {
		this.clientName = clientName;
	}

	public void setEmail( String email ) {
		this.email = email;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public void setPassword( String password ) {
		this.password = password;
	}
}
